package com.test.tabledetails.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReaderBuilder;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.test.tabledetails.model.NamedColumnBean;

import lombok.extern.slf4j.Slf4j;



@Service
@Slf4j
public class CSVFileProcessServiceImpl implements CSVFileProcessService {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	
	@Value("${config.localFilePath}")
	private String localFilePath;

	@Value("${config.finalFilePath}")
	private String tableDetailsFilePath;

	@Override
	public void processCSVFile() {
		String strPath = localFilePath;
		Path path = Paths.get(strPath);
		parseAndInsert(path); 
		path = Paths.get(tableDetailsFilePath);
		//deleteFile(path); // Delete file if already exist in the path.
	}
	
	
	private void parseAndInsert(Path path) {
		FileWriter writer;
        try {
            log.info("parsing file {}", path.toAbsolutePath());
            List<NamedColumnBean> txList = parseCSV(path, NamedColumnBean.class); // Parsing using CSV file.
            Map<String,Long> tableNameCounter = CSVFileProcessServiceImpl.tableNameCounter(txList);
            
            System.out.println("Counters - " + tableNameCounter);
            
            
			writer = new FileWriter(tableDetailsFilePath, false); 
			int i = 0;
			File f = new File(tableDetailsFilePath);
			if(!f.exists()){
			  f.createNewFile();
			}else{
				log.info("File already exists");
			}
			for (Map.Entry<String, Long> kvp : tableNameCounter.entrySet()) {
				// To add column name
				if(i == 0) {
					writer.write("TABLENAME");
					writer.write(",");
					writer.write("COUNT");
					writer.write("\r\n");
					i++;
				}
				writer.write(kvp.getKey());
				writer.write(",");
				writer.write(kvp.getValue().toString());
				writer.write("\r\n");
        	    
        	    
        	}
			writer.close();
            }  catch (Exception e) {
                    log.error("error while inserting {}", e.getMessage());
            } finally {
            	
            }
            	
    }
	
	@Override
    public List<NamedColumnBean> parseCSV(Path path, Class clazz) throws Exception {
		try (Reader reader = Files.newBufferedReader(path)) {
			final RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();
			final CSVReaderBuilder csvReaderBuilder = new CSVReaderBuilder(reader)
			    .withCSVParser(rfc4180Parser);
			CsvToBean cb = new CsvToBeanBuilder(csvReaderBuilder.build()).withType(clazz)
					.withIgnoreEmptyLine(true).withFieldAsNull(CSVReaderNullFieldIndicator.BOTH).
					//withEscapeChar('\\').
					withSeparator(',').
					withIgnoreLeadingWhiteSpace(true)
					//withSkipLines(1)
					.build();
			List<NamedColumnBean> csvList = new LinkedList<>();
			csvList.addAll(cb.parse());
			log.info("csv file parsed has size {}", csvList.size());
			return csvList;
		} catch (Exception e) {
			log.error("Error in parsing CS csv", e);
			throw e;
		}
		 
    }
	
    
    private void deleteFile(Path path) {
        log.info("deleting file {}", path.toAbsolutePath());

        try {
            Files.delete(path);
        } catch (IOException e) {
            log.error("error deleting file {}", e.getMessage());
        }
    }
    
    
    static Map<String,Long> tableNameCounter(List<NamedColumnBean> lstTableInfo) {
        // Creating a HashMap containing tableName
        // as a key and occurrences as  a value
        Map<String, Long> tableNameCounterMap = new LinkedHashMap<String, Long>();
 
        for (NamedColumnBean bean : lstTableInfo) {
        	if(Optional.ofNullable(bean.getTableName()).isPresent() && bean.getCount().trim() != "") {
        		if (tableNameCounterMap.containsKey(bean.getTableName())) {
        			 
                    // If tableName  is present in tableNameCounterMap,
                    // incrementing it's count by addition of previous
                	tableNameCounterMap.put(bean.getTableName(), tableNameCounterMap.get(bean.getTableName()) + Long.parseLong(bean.getCount()));
                }
                else {
     
                    // If tableName is not present in tableNameCounterMap,
                    // putting this tableName to tableNameCounterMap with  it's value
                	tableNameCounterMap.put(bean.getTableName(), Long.parseLong(bean.getCount()));
                }
        	}
            
        }
        // Sorting map by descending order.
        LinkedHashMap<String, Long> sortedMap = tableNameCounterMap.entrySet().stream()
				.sorted((i1, i2) -> i2.getValue().compareTo(i1.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        
        return sortedMap;
 
    }
    
}
