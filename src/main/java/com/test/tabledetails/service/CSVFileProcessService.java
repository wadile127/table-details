package com.test.tabledetails.service;

import java.nio.file.Path;
import java.util.List;

import com.test.tabledetails.model.NamedColumnBean;

public interface CSVFileProcessService {
	void processCSVFile();
	List<NamedColumnBean> parseCSV(Path path, Class clazz) throws Exception;

}
