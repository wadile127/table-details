package com.test.tabledetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TableDetailsApplication.class, args);
	}

}
