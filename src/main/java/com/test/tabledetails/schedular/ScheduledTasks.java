package com.test.tabledetails.schedular;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.tabledetails.service.CSVFileProcessService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ScheduledTasks {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    @Autowired
    CSVFileProcessService csvFileProcessService;
    
    //@Scheduled(cron = "${cron-expression.beneficiarymigration}")
    //@Scheduled(fixedRate = 1000)
    public void performTaskUsingCron() {
    	log.info("Regular task performed using Cron for CSVFile processing at " + dateFormat.format(new Date()));
    	Instant start = Instant.now();
    	csvFileProcessService.processCSVFile();
    	Instant end = Instant.now();
    	Duration timeElapsed = Duration.between(start, end);
    	log.info("Total time taken by CSVFile processing is : " + timeElapsed.toMinutes() + " minutes");
    }

}
