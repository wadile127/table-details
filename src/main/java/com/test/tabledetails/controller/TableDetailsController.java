package com.test.tabledetails.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.test.tabledetails.model.NamedColumnBean;
import com.test.tabledetails.service.CSVFileProcessService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin
public class TableDetailsController {

	@Autowired
	private CSVFileProcessService csvFileProcessService;
	
	@Value("${config.finalFilePath}")
	private String finalFilePath;


	@PostMapping(path = "/processCSVFiles")
	public ResponseEntity<String> processCSVFiles(@RequestHeader(required = false) String Authorization) {
		Instant start = Instant.now();
		csvFileProcessService.processCSVFile();
		Instant end = Instant.now();
		Duration timeElapsed = Duration.between(start, end);
		log.info("Total time taken by CSVFile processing is : " + timeElapsed.toMinutes() + " minutes");

		return new ResponseEntity<>("CSV File Imported", HttpStatus.CREATED);
	}

	
	@GetMapping("/download-tableDetails")
    public void exportCSV(HttpServletResponse response) throws Exception {

        //set file name and content type
        String filename = "tableDetails.csv";
        
        Path path = Paths.get(finalFilePath);

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<NamedColumnBean> writer = new StatefulBeanToCsvBuilder<NamedColumnBean>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(true)
                .build();

        //write all tableDetails to csv file
        writer.write(csvFileProcessService.parseCSV(path, NamedColumnBean.class));
	}
	
}