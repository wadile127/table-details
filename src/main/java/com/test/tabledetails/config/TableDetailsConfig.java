package com.test.tabledetails.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Configuration class for Swagger and other utility classes
 * @author Manoj Wadile
 *
 */
@Configuration
public class TableDetailsConfig {
	@Bean
	public Docket produceApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.test.tabledetails.controller")).build();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}