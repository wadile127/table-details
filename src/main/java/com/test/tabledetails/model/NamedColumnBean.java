package com.test.tabledetails.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CSV file NamedColuman Bean class
 * @author Manoj Wadile
 *
 */
public class NamedColumnBean extends CsvBean {
	//	TABLENAME	COUNT


    @CsvBindByName(column = "TABLENAME")
	//@CsvBindByPosition(position = 0)
    private String tableName;
    
    @CsvBindByName(column = "COUNT")
	//@CsvBindByPosition(position = 1)
    private String count;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
    
   

	
    
}
